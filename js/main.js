$(function () {
    $("#search-input").keyup(function () {
        var searchString = $(this).val();
        console.log(searchString);
        $('#jstree').jstree('search', searchString);
    });

    var treeData = [
        {
            "id": "1.0",
            "text": "AMS",
            "icon": "",
            "state": {
                "opened": false,
                "disabled": false,
                "selected": false
            },
            "children": [{
                "id": "2.06.0",
                "text": "care creation",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "data": {
                    "type": "png",
                    "name": "care-creation.png"
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null,

            }, {
                "id": "2.07.0",
                "text": "CSR",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "data": {
                    "type": "png",
                    "name": "csr.png"
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }, {
                "id": "2.08.0",
                "text": "owrite",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false,
                },
                "data": {
                    "type": "pdf",
                    "name": "owrite.pdf"
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }, {
                "id": "2.09.0",
                "text": "Trust",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "data": {
                    "type": "pdf",
                    "name": "trust.pdf"
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }, {
                "id": "2.010.0",
                "text": "Out of warranty",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "data": {
                    "type": "png",
                    "name": "warranty.png"
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }],
            "liAttributes": null,
            "aAttributes": null
        }, {
            "id": "3.0",
            "text": "EMEA",
            "icon": "",
            "state": {
                "opened": false,
                "disabled": false,
                "selected": false
            },
            "children": [{
                "id": "3.06.0",
                "text": "care creation",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }, {
                "id": "3.07.0",
                "text": "CSR",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }, {
                "id": "3.08.0",
                "text": "owrite",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }, {
                "id": "3.09.0",
                "text": "Exception",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }],
            "liAttributes": null,
            "aAttributes": null
        }, {
            "id": "4.0",
            "text": "SPR",
            "icon": "",
            "state": {
                "opened": false,
                "disabled": false,
                "selected": false
            },
            "children": [{
                "id": "4.06.0",
                "text": "care creation",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }, {
                "id": "4.07.0",
                "text": "CSR",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }, {
                "id": "4.08.0",
                "text": "owrite",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }, {
                "id": "4.09.0",
                "text": "Exception",
                "icon": "",
                "state": {
                    "opened": false,
                    "disabled": false,
                    "selected": false
                },
                "children": false,
                "liAttributes": null,
                "aAttributes": null
            }],
            "liAttributes": null,
            "aAttributes": null
        }
    ];
    var jstreeInfo = {
        'core': {
            'data': treeData
        },
        "search": {

            "case_insensitive": true,
            "show_only_matches": true
        },
        "plugins": ["search"]
    };

    $('#jstree').jstree(jstreeInfo);

    $("#jstree").bind("select_node.jstree",
        function (evt, data) {
            console.log(data.node);
            fileInfo = data.node.data
            if (fileInfo) {
                if (fileInfo.type === "png") {
                    var imagePath = "./files/images/" + fileInfo.name
                    $("#child-info").html("<img src=\'" + imagePath + "\'/>");
                } else if (fileInfo.type === "pdf") {
                    var pdfPath = "./files/pdf/" + fileInfo.name
                    $("#child-info").html("<embed src=\'" + pdfPath + "\' width=\"800px\" height=\"2100px\"/>");
                }
            }
        }
    );
});